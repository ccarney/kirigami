# Wantoyo <wantoyek@gmail.com>, 2018, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2022-09-27 23:06+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"
"X-Qt-Contexts: true\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Kirim sebuah email ke %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "Ikut Terlibat"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr ""

#: controls/AboutItem.qml:221
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Report Bug…"
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "Laporkan Bug..."

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Hak Cipta"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "Lisensi:"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Lisensi: %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Pustaka lib yang digunakan"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Penulis"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "Tampilkan foto penulis"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Kredit"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Penerjemah"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "Tentang %1"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "Berhenti"

#: controls/ActionToolBar.qml:192
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Aksi Selebihnya"

#: controls/Avatar.qml:177
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr "Hapus Tag"

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Aksi"

#: controls/GlobalDrawer.qml:504
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Mundur"

#: controls/GlobalDrawer.qml:596
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Tutup Bilah Sisi"

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Buka Bilah Sisi"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr "Memuat..."

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Password"

#: controls/PasswordField.qml:45
#, fuzzy
#| msgctxt "PasswordField|"
#| msgid "Password"
msgctxt "PasswordField|"
msgid "Hide Password"
msgstr "Password"

#: controls/PasswordField.qml:45
#, fuzzy
#| msgctxt "PasswordField|"
#| msgid "Password"
msgctxt "PasswordField|"
msgid "Show Password"
msgstr "Password"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
#, fuzzy
#| msgctxt "OverlayDrawer|"
#| msgid "Close drawer"
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Tutup penggambar"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr ""

#: controls/SearchField.qml:86
msgctxt "SearchField|"
msgid "Search…"
msgstr "Cari..."

#: controls/SearchField.qml:88
msgctxt "SearchField|"
msgid "Search"
msgstr "Cari"

#: controls/SearchField.qml:99
msgctxt "SearchField|"
msgid "Clear search"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "Pengaturan"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "Pengaturan — %1"

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Tutup penggambar"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr "Buka penggambar"

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Navigasi Mundur"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Navigasi Maju"

#: controls/UrlButton.qml:34
#, qt-format
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link %1"
msgstr ""

#: controls/UrlButton.qml:35
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link"
msgstr ""

#: controls/UrlButton.qml:58
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr "Salin Tautan ke Papan Klip"

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "Sistem perjendelaan %1"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (dibangun terhadap %3)"

#~ msgctxt "PageTab|"
#~ msgid "Current page. Progress: %1 percent."
#~ msgstr "Halaman saat ini. Progres: %1 persen."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Progress: %2 percent."
#~ msgstr "Navigasi ke %1. Progres: %2 persen."

#~ msgctxt "PageTab|"
#~ msgid "Current page."
#~ msgstr "Halaman saat ini."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Demanding attention."
#~ msgstr "Navigasi ke %1. Memerlukan perhatian."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1."
#~ msgstr "Navigasi ke %1."

#~ msgctxt "ToolBarApplicationHeader|"
#~ msgid "More Actions"
#~ msgstr "Aksi Selebihnya"

#~ msgctxt "AboutItem|"
#~ msgid "Visit %1's KDE Store page"
#~ msgstr "Kunjungilah halaman KDE Store %1"

#~ msgctxt "UrlButton|"
#~ msgid "Copy link address"
#~ msgstr "Salin alamat tautan"

#~ msgctxt "SearchField|"
#~ msgid "Search..."
#~ msgstr "Cari..."

#~ msgctxt "AboutPage|"
#~ msgid "%1 <%2>"
#~ msgstr "%1 <%2>"

#~ msgctxt "ToolBarPageHeader|"
#~ msgid "More Actions"
#~ msgstr "Aksi Selebihnya"
