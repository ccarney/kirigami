# giovanni <g.sora@tiscali.it>, 2017, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2023-06-23 15:55+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Invia un message de e-posta a  %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "Sea involvite"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr "Dona"

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "Reporta un bug"

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Copyright"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "Licentia:"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Licentia: %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Bibliothecas in uso"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Autores"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "Monstra photos de autor"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Gratias"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Traductores"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "A proposito de %1"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "Abandona"

#: controls/ActionToolBar.qml:192
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Ulterior Actiones"

#: controls/Avatar.qml:177
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr "Remove etiquetta"

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Actiones"

#: controls/GlobalDrawer.qml:504
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Retro"

#: controls/GlobalDrawer.qml:596
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Claude barra lateral"

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Aperi barra lateral"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr "Cargante..."

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Contrasigno"

#: controls/PasswordField.qml:45
msgctxt "PasswordField|"
msgid "Hide Password"
msgstr "Cela Contrasigno"

#: controls/PasswordField.qml:45
msgctxt "PasswordField|"
msgid "Show Password"
msgstr "Monstra Contrasigno"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Claude menu"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr "Aperi menu"

#: controls/SearchField.qml:86
msgctxt "SearchField|"
msgid "Search…"
msgstr "Cerca…"

#: controls/SearchField.qml:88
msgctxt "SearchField|"
msgid "Search"
msgstr "Cerca"

#: controls/SearchField.qml:99
msgctxt "SearchField|"
msgid "Clear search"
msgstr "Netta cerca"

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "Preferentias"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "Preferentias — %1"

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Claude designator"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr "Aperi designator"

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Navigation de retro"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Naviga Avante"

#: controls/UrlButton.qml:34
#, qt-format
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link %1"
msgstr "Aperi ligamine %1"

#: controls/UrlButton.qml:35
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link"
msgstr "Aperi ligamine"

#: controls/UrlButton.qml:58
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr "Copia ligame a area de transferentia"

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "Le %1 systema de fenestra"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (construite sur %3)"

#~ msgctxt "PageTab|"
#~ msgid "Current page. Progress: %1 percent."
#~ msgstr "Pagina currente. Progresso: %1 percent"

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Progress: %2 percent."
#~ msgstr "Naviga a %1. Progresso: %2 percent."

#~ msgctxt "PageTab|"
#~ msgid "Current page."
#~ msgstr "Pagina currente."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Demanding attention."
#~ msgstr "Navigante a %1. Demandante attention."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1."
#~ msgstr "Naviga a %1"

#~ msgctxt "ToolBarApplicationHeader|"
#~ msgid "More Actions"
#~ msgstr "Ulterior Actiones"

#~ msgctxt "AboutItem|"
#~ msgid "Visit %1's KDE Store page"
#~ msgstr "Visita pagina %1 de KDE Store"

#~ msgctxt "UrlButton|"
#~ msgid "Copy link address"
#~ msgstr "Copia adresse de ligamine"

#~ msgctxt "LoadingPlaceholder|"
#~ msgid "Still loading, please wait."
#~ msgstr "Ancora cargante, pro favor, tu attende."

#~ msgctxt "AboutItem|"
#~ msgid "(%1)"
#~ msgstr "(%1)"

#~ msgctxt "SearchField|"
#~ msgid "Search..."
#~ msgstr "Cerca..."

#~ msgctxt "AboutPage|"
#~ msgid "%1 <%2>"
#~ msgstr "%1 <%2>"

#, fuzzy
#~| msgctxt "ContextDrawer|"
#~| msgid "Actions"
#~ msgctxt "ToolBarPageHeader|"
#~ msgid "More Actions"
#~ msgstr "Actiones"
